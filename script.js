

class Employee{
    constructor(name, age, salary ){
        this._name = name
        this._age = age
        this._salary = salary
    }
    get name(){
        return this._name;
    }

    set name(newName){
        this._name = newName;
    }
    get age(){
        return this._age;
    }

    set age(newAge){
        this._age = newAge;
    }
    get salary() {
        return this._salary;
    }

  set salary(newSalary) {
    this._salary = newSalary;
  }
   
}
class Programmer extends Employee{
    constructor(name, age , salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }
    get salary(){
        return this._salary * 3;
    }
}

const user1 = new Programmer("Alex", 23, 1400,"JavaScript");

console.log("Ім'я:", user1.name);
console.log("Вік:", user1.age);
console.log("Зарплата:", user1.salary);


const user2 = new Programmer("Mary", 28, 900, "Java");

console.log("Ім'я:", user2.name);
console.log("Вік:", user2.age);
console.log("Зарплата:", user2.salary);
console.log("Мова програмування:", user2.lang)




